# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
import datetime
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, ModelWorkflow, fields
from trytond.wizard import Wizard
from trytond.pyson import Eval, Bool, Get, Not, Or, Equal, In
from trytond.transaction import Transaction
from trytond.modules.contract.contract import _STATES, _DEPENDS
from trytond.pool import Pool


_CONTRACT_INVOICE_METHODS = [
    ('no_invoice', 'No Invoicing'),
    ('work', 'Work Associated'),
    ('fix', 'Fixed Price'),
    ('recurring', 'Recurring')
]

class Contract(ModelWorkflow, ModelSQL, ModelView):
    _name = 'contract.contract'

    billing_lines = fields.One2Many('account.invoice.billing_line',
            'contract', 'Billing Lines', domain=[
                ('party', '=', Eval('party_invoice'))
            ], states={
                'readonly': True
            }, depends=['party_invoice'])
    invoice_method = fields.Selection(_CONTRACT_INVOICE_METHODS, 'Invoice Method',
            required=True, states=_STATES, depends=_DEPENDS)

    def __init__(self):
        super(Contract, self).__init__()
        self.invoice_address = copy.copy(self.invoice_address)
        if self.invoice_address.states:
            self.invoice_address.states = copy.copy(
                    self.invoice_address.states)
            self.invoice_address.states['invisible'] = False
        self.payment_term = copy.copy(self.payment_term)
        if self.payment_term.states:
            self.payment_term.states = copy.copy(self.payment_term.states)
            self.payment_term.states['invisible'] = False
        self.invoice_type = copy.copy(self.invoice_type)
        if self.invoice_type.states:
            self.invoice_type.states = copy.copy(self.invoice_type.states)
            self.invoice_type.states['invisible'] = False
        self.lines = copy.copy(self.lines)
        if self.lines.states:
            self.lines.states = copy.copy(self.lines.states)
            if 'readonly' in self.lines.states:
                self.lines.states['readonly'] = Or(self.lines.states['readonly'],
                        Bool(In(Eval('invoice_method'), ['work', 'no_invoice'])))
            else:
                self.lines.states['readonly'] = Bool(In(
                        Eval('invoice_method'), ['work', 'no_invoice']))
        else:
            self.lines.states = {'readonly': Bool(In(
                    Eval('invoice_method'), ['work', 'no_invoice']))}
        if 'invoice_method' not in self.lines.depends:
            self.lines.depends = copy.copy(self.lines.depends)
            self.lines.depends.append('invoice_method')
        self._reset_columns()

        self._error_messages.update({
            'invoiced_confirmed_billing_lines': 'Contracts with billing lines '
                'in state "Invoiced" or "Confirmed" can not be canceled!',
            'append_invoice_method_fix': 'You can add invoices only once '
                'to a contract with an invoice method equals to "fix"!',
            'open_billing_lines': 'Contracts with billing lines that are '
                'not yet invoiced can not be terminated!',
            'bill_only_active_contract': 'Billing is only possible on '
                "contracts with state 'Active'!\n\nAffected contract: %s (%s)",
            })


    def default_invoice_method(self):
        return 'work'

    def default_parties(self):
        res = []
        if Transaction().context.get('party'):
            res.append({
                'party': Transaction().context['party']
            })
        return res

    def default_invoice_address(self):
        party_obj = Pool().get('party.party')
        if Transaction().context.get('party'):
            return party_obj.address_get(Transaction().context['party'],
                    type='invoice')
        return False

    def copy(self, ids, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['billing_lines'] = False
        return super(Contract, self).copy(ids, default=default)

    def bill(self, contract):
        '''
        Contract.bill(contract)

        Proceed all billing lines with state 'confirmed' of a contract to
        invoices.
        :param contract: the contract object or id to bill

        :return: a list of ids from the generated invoices
        '''
        billing_line_obj = Pool().get('account.invoice.billing_line')

        if isinstance(contract, (int, long)):
            contract = self.browse(contract)

        if contract.state != 'active':
            self.raise_user_error('bill_only_active_contract',
                error_args=(contract.name, contract.code))

        invoice_ids = []
        # For invoice_method 'fix' only one set of invoice can be created
        if contract.invoice_method == 'fix':
            if contract.invoices:
                self.raise_user_error('append_invoice_method_fix')
            else:
                invoice_ids = self.create_invoice(contract)
                self.write(contract.id, {
                    'end_date': datetime.datetime.now(),
                    })
        elif contract.invoice_method == 'work':
            billing_line_ids = billing_line_obj.search([
                ('contract', '=', contract.id),
                ('state', '=', 'confirmed')])
            if billing_line_ids:
                invoice_ids = billing_line_obj.create_invoices(billing_line_ids)
                if invoice_ids:
                    self.write(contract.id, {'invoices': [('add', invoice_ids)]})

        return invoice_ids

    def check_terminate(self, contract_id):
        # all billing lines must be invoiced to *terminate* a contract
        res = super(Contract, self).check_terminate(contract_id)
        if res:
            contract = self.browse(contract_id)
            if contract.billing_lines:
                for line in contract.billing_lines:
                    if line.state != 'invoice':
                        self.raise_user_error('open_billing_lines')
        return res

    def check_cancel(self, contract_id):
        billing_line_obj = Pool().get('account.invoice.billing_line')

        res = super(Contract, self).check_cancel(contract_id)
        contract = self.browse(contract_id)
        billing_lines = contract.billing_lines
        billing_line_ids = [x.id for x in billing_lines if x.state in
                ('invoice', 'confirmed')]
        if billing_line_ids:
            self.raise_user_error('invoiced_confirmed_billing_lines')
        else:
            billing_line_obj.delete([x.id for x in billing_lines])

        return res

Contract()


class ContractParty(ModelSQL, ModelView):
    _name = 'contract.party'

    def __init__(self):
        super(ContractParty, self).__init__()
        self.type = copy.copy(self.type)
        if self.type.states:
            if self.type.states.get('required'):
                self.type.states['required'] = Or(self.type.states['required'],
                        Equal(Get(Eval('_parent_contract', {}),
                                'invoice_method'), 'fix'))
            else:
                self.type.states['required'] = Equal(Get(Eval(
                        '_parent_contract', {}), 'invoice_method'), 'fix')
        else:
            self.type.states = {'required': Equal(Get(Eval(
                    '_parent_contract', {}), 'invoice_method'), 'fix')}
        self._reset_columns()

    def default_type(self):
        party_type_obj = Pool().get('contract.party.type')
        party_type_ids = party_type_obj.search([
                ('code', '=', 'invoice_recipient')
                ], limit=1)
        return party_type_ids[0]

ContractParty()


class CreateInvoice(Wizard):
    _name = 'contract.create_invoice'

    states = {

        'init': {
            'result': {
                'type': 'action',
                'action': '_action_create_invoices',
                'state': 'end',
                },
            },
        }

    def _action_create_invoices(self, data):
        pool = Pool()
        contract_obj = pool.get('contract.contract')
        invoice_obj = pool.get('account.invoice')
        model_data_obj = pool.get('ir.model.data')
        act_window_obj = pool.get('ir.action.act_window')

        all_invoice_ids = []
        for contract in contract_obj.browse(data['ids']):
            invoice_ids = contract_obj.bill(contract)
            all_invoice_ids.extend(invoice_ids)

        args = [('id', 'in', all_invoice_ids)]
        invoice_ids = invoice_obj.search(args)

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_invoice_out_invoice_form'),
            ('module', '=', 'account_invoice'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)
        res['domain'] = str([
            ('id', 'in', invoice_ids),
            ])
        if len(invoice_ids) == 1:
            res['res_id'] = invoice_ids[0]
            res['views'].reverse()
        return res

CreateInvoice()
