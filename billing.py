# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import copy
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Bool, Eval, Or
from trytond.modules.account_invoice_billing.billing import _STATES, _DEPENDS
from trytond.pool import Pool


class BillingLine(ModelSQL, ModelView):
    _name = 'account.invoice.billing_line'

    contract = fields.Many2One('contract.contract', 'Contract',
            ondelete='RESTRICT', domain=[('party_invoice', '=', Eval('party'))],
            on_change=['contract'], states=_STATES, depends=_DEPENDS+['party'])

    def __init__(self):
        super(BillingLine, self).__init__()
        self._error_messages.update({
            'active_contract': "You cannot create or modify a billing line "
                "for a contract that is not in state 'active'. "
                "Affected contract: %s (%s)"})
        self.state = copy.copy(self.state)
        if not self.state.selection:
            self.state.selection = []
        if ('confirmed', 'Confirmed') not in self.state.selection:
            self.state.selection += [('confirmed', 'Confirmed')]
        self.party = copy.copy(self.party)
        if self.party.on_change is None:
            self.party.on_change = []
        if 'party' not in self.party.on_change:
            self.party.on_change += ['party']
        self._reset_columns()
        self._rpc.update({
                'on_change_party': False,
                })

    def on_change_party(self, vals):
        res = {}
        if not vals.get('party'):
            res['contract'] = False
        return res

    def on_change_contract(self, vals):
        res = {'state': 'draft'}
        if vals.get('contract'):
            res['state'] = 'confirmed'
        return res

    def _update_invoice(self, invoice, invoice_values, billing_line):
        billing_date = billing_line.billing_date
        if billing_date < invoice.time_of_supply_start:
            invoice_values.setdefault('time_of_supply_start', billing_date)
        elif billing_date > invoice.time_of_supply_end:
            invoice_values.setdefault('time_of_supply_end', billing_date)
        return super(BillingLine, self)._update_invoice(invoice,
            invoice_values, billing_line)

    def _create_invoice(self, invoice_values, billing_line):
        billing_date = billing_line.billing_date
        invoice_values.setdefault('time_of_supply_start', billing_date)
        invoice_values.setdefault('time_of_supply_end', billing_date)
        if billing_line.contract:
            invoice_values.setdefault('reference', billing_line.contract.code)
            if billing_line.contract.payment_term:
                invoice_values['payment_term'] = (
                    billing_line.contract.payment_term.id)
        return super(BillingLine, self)._create_invoice(invoice_values,
            billing_line)

    def check_modify_line_contract(self, contract_id, raise_exception=False):
        contract_obj = Pool().get('contract.contract')
        contract = contract_obj.browse(contract_id)
        if contract.state != 'active':
            if raise_exception:
                self.raise_user_error('active_contract',
                        error_args=(contract.name, contract.code))
            return False
        return True

    def create(self, vals):
        if vals.get('contract'):
            self.check_modify_line_contract(vals['contract'],
                    raise_exception=True)
        return super(BillingLine, self).create(vals)

    def write(self, ids, vals):
        if vals.get('contract'):
            if not self.check_modify_line_contract(vals['contract'],
                    raise_exception=False):
                return False
        return super(BillingLine, self).write(ids, vals)

    def delete(self, ids):
        for line in self.browse(ids):
            if line.contract:
                self.check_modify_line_contract(line.contract.id,
                    raise_exception=True)
        return super(BillingLine, self).delete(ids)

    def _set_state_on_delete_invoice_line(self, billing_line_ids):
        self.write(billing_line_ids, {'state': 'confirmed'})

BillingLine()
