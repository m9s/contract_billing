# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Contract Billing',
    'name_de_DE': 'Vertragsverwaltung Abrechnung',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds billing to contracts
    ''',
    'description_de_DE': '''
    - Fügt Abrechnung zu Verträgen hinzu.
    ''',
    'depends': [
        'account_invoice_billing',
        'contract_automation_invoice'
    ],
    'xml': [
        'contract.xml',
        'billing.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
